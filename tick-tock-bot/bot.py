#!/bin/env python
import discord

import os
from os import path

from subprocess import run, Popen, PIPE


def file_to_string(filename):
    """reads contents of a file in as a string"""
    with open(filename) as infile:
        return infile.read().strip()

QWERNOMIC_DIR="qwernomic-sigilizer"
QWER_EXECUTABLE=f"{QWERNOMIC_DIR}/qwer"
TOKEN=file_to_string("token.txt")
CMD="!QWER "
CMD_LEN=len(CMD)

def toupperalnum(text):
    txt = text.upper()
    return "".join([c for c in txt if c in "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"])

def run_qwer(text):
    """returns the filename of the new png or None"""
    input_text = toupperalnum(text)
    if len(input_text) == 0:
        return None
    else:
        proc = Popen([QWER_EXECUTABLE, input_text], stdout=PIPE)
        output, err = proc.communicate()

    if proc.returncode == 1:
        return None
    else:
        return output.decode('ascii')


intents = discord.Intents.default()
intents.message_content = True
client = discord.Client(intents=intents)

@client.event
async def on_message(msg):
    """"""
    # return if we make this message ourself
    if msg.author == client.user:
        return

    content = msg.content.upper()
    command = content[:CMD_LEN]
    text = content[CMD_LEN:]

    print("content:", content)
    print("command:", content[:CMD_LEN])
    print("text:", content[CMD_LEN:])
    #do command
    if command == CMD:
        # too many character,,, 100 is arbitrary idk
        if (len(text) > 100):
            await msg.channel.send(f"too many characters, limit is {100}")
            return

        filename = run_qwer(text)
        # first case, output is bad
        # notify the user
        if filename is None:
            await msg.channel.send("Invalid input?")
        # otherwise, it's showtime!
        else:
            # send the file and say what the phrase is
            await msg.channel.send(content[CMD_LEN:], file=discord.File(filename))
            # delete the file so i don't get a million pngs
            os.remove(filename)
    else:
        pass
    

def main():
    client.run(TOKEN)

if __name__ == "__main__":
    main()
