# tick tock, tick tock

9::3, coincidence absent any numerical link

This project requires:
- the `qwer` executable at `qwernomic-sigilizer/qwer`
- a file named `token.txt` containing a discord bot token

## SETUP:

```sh
python -m venv env
source env/bin/activate
pip install -r requirements.txt
```

## RUNNING:
```sh
source env/bin/activate
python bot.py
```
