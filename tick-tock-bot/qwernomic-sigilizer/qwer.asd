(in-package :asdf-user)

(defsystem "qwer"
  :name "qwer"
  :description "qwernomic sigilizer application"
  :author "anon"
  :license "MIT"
  :version "0.0.0"
  :depends-on ("imago" "alexandria")
  :components ((:file "implementation"))
  :build-operation "program-op" ;; leave as is
  :build-pathname "qwer"
  :entry-point "qwer:main"
  )
