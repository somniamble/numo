;; (ql:quickload :sketch)
;; (defpackage :qwer (:use :cl :sketch))
;; (in-package :qwer)
;; (ql:quickload :imago :silent t)
(defpackage :qwer
  ;; (:use :cl :imago :lispcord)
  (:use #:cl #:imago)
  (:import-from #:alexandria
		#:symbolicate)
  (:export ;; #:text-to-filename
	   ;; #:draw-sigil
	   #:main))
(in-package :qwer)
(require :uiop)
;; (ql:quickload :alexandria :silent t)

(defparameter *row-one* '(#\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9 #\0))
(defparameter *row-two* '(#\Q #\W #\E #\R #\T #\Y #\U #\I #\O #\P))
(defparameter *row-three* '(#\A #\S #\D #\F #\G #\H #\J #\K #\L))
(defparameter *row-four* '(#\Z #\X #\C #\V #\B #\N #\M))

(defparameter *box-size* '(48 48))
(defparameter *box-center-offset* '(24 24))

(defparameter *row-one-offset* '(0 0))
(defparameter *row-two-offset* '(30 48))
(defparameter *row-three-offset* '(42 96))
(defparameter *row-four-offset* '(72 144))


(defun translate-vertex (vertex by-vertex)
  "translates a vertex by a certain magnitude in two dimensions"
  (list (+ (car vertex) (car by-vertex)) (+ (cadr vertex) (cadr by-vertex))))

(defun negate-vertex (vertex)
  "negates the x and y coordinate of a vertex"
  (list (- (get-x vertex)) (- (get-y vertex))))

(defun get-x (vertex)
  "gets the x component of a two-dimensional vertex"
  (car vertex))

(defun get-y (vertex)
  "gets the y component of a two-dimensional vertex"
  (cadr vertex))

(defun take (n lst)
  "takes the first n elements from a list"
  (if (> n (length lst)) lst
      (subseq lst 0 n)))

(defun range (start end &optional (step 1))
  "collects the range of numbers on the inclusive interval (start, end)"
  (loop for i from start
	by step
	  to end
	collect i))

(defun step-from (start step count)
  "counts up n elements from start stepwise"
  (loop repeat count
	for value = start
	  then (+ value step)
	collect value))

(defun repeat-value (n count)
  "generates a list of n count times"
  (loop for i from 0 to count
	collect n))

(defun zip (&rest more-lists)
  "zips up two or more lists into pairs, triples, etc... depending on the number of lists."
  (apply #'mapcar #'list more-lists))

(defun generate-vertices (start-xy offset-xy count)
  "generates a list of 2 dimensional vertices"
  (zip (step-from (get-x start-xy) (get-x offset-xy) count)
       (step-from (get-y start-xy) (get-y offset-xy) count)))

(defun make-row-offsets (row-offset count)
  "Generates a list of offsets for a given keyboard row"
  (generate-vertices (translate-vertex row-offset *box-center-offset*)
		     (list (car *box-size*) 0)
		     count))

(defun generate-keys (letters row-offset)
  "zips together a list of letters and their positions on the keyboard"
  (zip letters (make-row-offsets row-offset (length letters))))


(defparameter *keys* (append (generate-keys *row-one* *row-one-offset*)
			     (generate-keys *row-two* *row-two-offset*)
			     (generate-keys *row-three* *row-three-offset*)
			     (generate-keys *row-four* *row-four-offset*)))

(defun fetch-key-position (character)
  "fetches the position of a key on the keyboard"
  (cadr (assoc character *keys*)))


(defun only-alphanum (text)
  "filters out any non-alpha-numeric characters from a string"
  (remove-if-not #'alphanumericp
		 (coerce text 'list)))

;; (defun char-to-symbol-or-digit (char)
;;   "turns a character into a symbol or a single digit"
;;   (if (alpha-char-p char)
;;       (symbolicate char)
;;       (digit-char-p char)))

(defun text-to-chars (text)
  "turns a string into a set of symbols and digits"
  (let ((massaged (only-alphanum (string-upcase text))))
    (coerce (only-alphanum massaged) 'list)
    ;; (mapcar #'char-to-symbol-or-digit 
    ;; 	    (coerce (only-alphanum massaged) 'list))
    ))

(defun fetch-positions (text)
  "fetches the list of key-positions corresponding to the letters and numbers in a string"
  (mapcar #'fetch-key-position (text-to-chars text)))

(defun points-to-lines (vertices)
  "turns a list of points from (x, y) vertices to (x1, y1, x2, y2) lines"
  (mapcar #'append vertices (cdr vertices)))

(defun draw-polyline (vertices image)
  "draws a list of vertices as a line on an image"
  (flet ((drawln (x1 y1 x2 y2)
		 (draw-line image x1 y1 x2 y2 +black+)))
	(loop for line-coordinates in (points-to-lines vertices)
	      do
		 (apply #'drawln line-coordinates))))

(defun symlist-to-string (symlist)
  (format nil "~{~a~}" symlist))

(defun bounding-box (vertices)
  "returns two vertices, indicating the top-leftmost vertext and
bottom-rightmost vertex containing the list of vertices"
  (labels ((helper (verts tl br)
	     "at each step, updates tl to the minimum coordinates of itself next vertex
updates br t othe maximum coordinates of itself and next vertex" 
	     (let ((v (car verts)))
	       (if (null verts) (list tl br)

		   (helper (cdr verts)
			   (list (min (get-x tl) (get-x v))
				 (min (get-y tl) (get-y v)))
			   (list (max (get-x br) (get-x v))
				 (max (get-y br) (get-y v))))))))
    (helper vertices '(10000000 10000000) '(0 0))))

(defun text-to-filename (text)
  (format nil "~a-sigil.png" (symlist-to-string (text-to-chars text))))

(defun draw-sigil (text &key (show-keyboard nil))
  (let* ((positions (fetch-positions text))
	 (bounds (bounding-box positions))
	 (tl-bound (translate-vertex (car bounds) '(-5 -5)))
	 (br-bound (translate-vertex (cadr bounds) '(10 10)))
	 (offset-positions (mapcar (lambda (v)
				     (translate-vertex v
						       (negate-vertex tl-bound)))
				   positions))
	 (image (if show-keyboard
		    (convert-to-rgb (read-image "qwerty.png"))
		    (make-instance 'rgb-image :width (get-x br-bound)
					      :height (get-y br-bound))))
	 (filename (text-to-filename text)))
    (if show-keyboard
	(draw-thicc-line image (positions-to-polyline positions)
			 +red+)
	(draw-thicc-line image (positions-to-polyline offset-positions)
			 +red+))
    (write-png image filename)))

(defun draw-thicc-line (image polyline color)
  (let* ((polylineup (mapcar #'1+ polyline))
	 (polylineupmore (mapcar #'1+ polylineup))
	 (polylinedown (mapcar #'1- polyline))
	 (polylinedownmore (mapcar #'1- polylinedown)))
    (draw-polygon image polyline color :closed nil)
    (draw-polygon image polylineup color :closed nil)
    (draw-polygon image polylinedown color :closed nil)
    (draw-polygon image polylineupmore color :closed nil)
    (draw-polygon image polylinedownmore color :closed nil)))



(defun positions-to-polyline (positions)
  (reduce #'append positions))

(defun main ()
  "gets text from command line, draws the sigil and prints the filename"
  (let ((args (uiop:command-line-arguments)))
    (if (null args)
	(progn (format t "please call this again with some arguments")
	       (sb-ext:exit :code 1))
	(let ((text (format nil "~{~a~}" args)))
	  (draw-sigil text)
	  (format t "~a" (text-to-filename text))))))
