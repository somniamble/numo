does a thing.

# BUILDING

## Dependencies

- [roswell](https://roswell.github.io/)
    - [Installation Guide](https://roswell.github.io/Installation.html)
- make
    - almost certainly provided by your distribution already, or installable from your package manager.

### Building Roswell from Source:
If you are on Debian/Ubuntu/Mint you will need to build roswell from source.

The following command is copied directly from the installation guide linked above:

```sh
sudo apt-get -y install git build-essential automake libcurl4-openssl-dev
git clone -b release https://github.com/roswell/roswell.git
cd roswell
sh bootstrap
./configure
make
sudo make install
ros setup
```

## Building QWER

Set up the build environment with roswell, and build the `qwer` ASDF system:
```sh
make environment
make build
```
This will produce the executable `qwer`

# RUNNING

```./qwer <some text here>```

if you run it with arguments, it should output the filename of the PNG it created`

if you run it without arguments then it will squawk at you and return exit code 1

